/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.usecu.script;

import com.corelationinc.script.Report;
import com.corelationinc.script.Script;
import com.corelationinc.script.ScriptException;
import com.corelationinc.script.Serial;
import com.corelationinc.script.XMLSerialize;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.stream.XMLStreamException;

/**
 *
 * @author rpatil
 */
public class HelocLienPosition {

    Script script = null;
    XMLSerialize xml = null;
    Connection connection = null;
    Report text_report = null;
    PrintStream text_os = null;
    
    String postingDate ;
    String data_file_name;
    String db_file_path;
    SM_Operation SM_OP;
    Serial targetSerial;
    CSVReader csvReader = null;
    
    private enum SM_Operation {
        Add
    };
    
    PreparedStatement helocLoanCollateral;
    
    private HelocLienPosition(Script value) {
        script = value;
        db_file_path = script.getDatabaseHomePathName() + "/import/";
        try {
            // open a text report and write some lines to it
            text_report = script.openReport("Heloc Lien Position - Text Report", Report.Format.txt);
        } catch (ScriptException ex) {
            Logger.getLogger(HelocLienPosition.class.getName()).log(Level.SEVERE, null, ex);
        }
        text_os = new PrintStream(text_report.getBufferedOutputStream());
        text_os.println("In Data Base Constructor.");
        text_os.println("DB File Path:'" + db_file_path + "'");

    }
    
    public static void runScript(Script script) throws Exception {
        HelocLienPosition updates = new HelocLienPosition(script);
        updates.run();
    }
    
    private boolean getArgs() {
        // retrieve the program arguments and do something with them
        Iterator<String> iterator = script.getArgumentIterator();
        String args;
        SM_OP = SM_Operation.Add;   // The default operation
        if (!iterator.hasNext()) {
            return false;
        }
        data_file_name = iterator.next();
        args = "\"" + data_file_name + "\"";

        while (iterator.hasNext()) {
            String arg = iterator.next();
            args += " " + arg;
        }
        return true;
    }
    
    private void run() throws Exception {

        if (!getArgs()) {
            throw new Exception("Script requires a file name as the first argument!");
        }
        
        csvReader = new CSVReader(db_file_path+data_file_name);
        csvReader.readFields();

        // open a database connection and get the current posting date
        connection = this.script.openDatabaseConnection();
        postingDate = this.script.retrievePostingDateString(connection);
        
        // open a text report and write some lines to it
        Report report = script.openReport("Heloc Lien Position report", Report.Format.txt);
        text_os = new PrintStream(report.getBufferedOutputStream());
        text_os.println("Report is running on: " + postingDate);
        text_os.println("Database is " + script.getDatabaseName() + ".");
        text_os.println("Database home path: " + script.getDatabaseHomePathName());       
        
        startQueryXML(); // open an XML posting report
        
        text_os.println("Div File: " + csvReader.csvData.size());
        String updatedAccountNumber= "";
        for (List<String> dataList : csvReader.csvData) {
            //Data File Variables
            String accountNumber = dataList.get(0);
            
            if(accountNumber.length() < 10){
            int accountNumberToInt = Integer.parseInt(accountNumber);    
                updatedAccountNumber = String.format("%010d", accountNumberToInt);
                accountNumber = updatedAccountNumber;
            }
  
            String loanId = dataList.get(1);           
            text_os.println("account Number before: " +accountNumber+ " Loan Id: " +loanId );
            String lienPosition = dataList.get(2);
            String loanCollateral = " SELECT LC.SERIAL FROM CORE.ACCOUNT AS A " +
                                " INNER JOIN CORE.LOAN AS L ON A.SERIAL = L.PARENT_SERIAL" +
                                " INNER JOIN CORE.COLLATERAL AS LC ON L.SERIAL = LC.PARENT_SERIAL" +
                                " WHERE L.CLOSE_DATE IS NULL AND A.ACCOUNT_NUMBER = '"+accountNumber+"' AND L.ID='"+loanId+"' ";                 
        
            helocLoanCollateral = connection.prepareStatement(loanCollateral);
            String serial = "";
            try{
                ResultSet resultSet = helocLoanCollateral.executeQuery();
                if(resultSet.next()){
                    serial = resultSet.getString(1);
                    targetSerial = new Serial(serial);
                    text_os.println("account Number:" +accountNumber+ " Target Serial:" +targetSerial + " Lien Position:" + lienPosition);            
                    lienPositionUpdate(lienPosition, targetSerial);
                }
            }finally{
                helocLoanCollateral.close();
            }
    
        }
           
        endQueryXML();  // Close an XML posting report
    }
    
    private void startQueryXML() throws Exception {
        // open an XML posting report
        Report xml_report = script.openReport("Data Scrubbing - Posting Report", Report.Format.xml);
        xml_report.setPostingOption(true);
        PrintStream xml_os = new PrintStream(xml_report.getBufferedOutputStream());

        // start the XML document
        xml = new XMLSerialize();
        xml.setXMLWriter(xml_os);
        xml.putStartDocument();
        xml.putBatchQuery(postingDate);
    }

    private void endQueryXML() throws Exception {
        xml.put();  // insert closing tag for 'query'
        xml.putEndDocument();  // end the XML document
    }
    
    public void lienPositionUpdate(String lienPostion , Serial targetSerial) throws SQLException, ScriptException, XMLStreamException{
        
        String lienOptions = "" ;
        if(lienPostion.equals("1")){
            lienOptions = "F";
        } else if(lienPostion.equals("2") || lienPostion.equals("3") ){
            lienOptions = "S";
        }
        
        xml.putSequence(); // <sequence>
        {
          xml.putTransaction(); // <transaction>
          {
            xml.put("exceptionDescriptionPrefix", "Exception target Serial: " +targetSerial);
            xml.putStep(); // <step>
            {
              xml.putRecord("COLLATERAL_record"); // <record>
              {
                xml.putOption("operation", "U");
                xml.put("tableName", "COLLATERAL");
                xml.put("targetSerial", targetSerial);
                xml.putOption("includeTableMetadata", "N");
                xml.putOption("includeColumnMetadata", "N");
                xml.putOption("includeRowDescriptions", "Y");
                xml.putOption("includeAllColumns", "N");

                xml.put("field"); // <field>
                {
                  xml.put("columnName", "OTH_MTG_CATEGORY");
                  xml.putOption("operation", "S");
                  xml.put("newContents", lienOptions);
                }
                xml.put(); // </field> 
              }
              xml.put(); // </record>
            }
            xml.put(); // </step>
          }
          xml.put(); // </transaction>
        }
        xml.put(); // </sequence>
    }
    
}
