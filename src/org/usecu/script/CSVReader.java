package org.usecu.script;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CSVReader {

    public List<List<String>> csvData;
    private final String filePath;
    private BufferedReader br = null;

    final String DELIMITER = ",";

    // Get the file path of the csv file to open.
    public CSVReader(String filePath) {
        this.filePath = filePath;
        this.csvData = new ArrayList();
    }

    public void readFields() throws IOException {

        try {
            String line = "";
            br = new BufferedReader(new FileReader(this.filePath));

            int count = 0;
            while ((line = br.readLine()) != null) {
                if (count > 0) {
                    String[] tokens = line.split(DELIMITER);
                    List<String> tempLine = new ArrayList<String>();
                    for (String token : tokens) {
                        if (!token.isEmpty()) {
                            tempLine.add(token);
                        } else {
                            tempLine.add("");
                        }
                    }
                    this.csvData.add(tempLine);
                }
                count++;
            }

        } catch (Exception e) {
        } finally {
            try {
                br.close();
            } catch (Exception e) {
            }
        }
    }
}